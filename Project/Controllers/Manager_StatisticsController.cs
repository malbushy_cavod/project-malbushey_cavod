﻿
using Newtonsoft.Json;
using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Project.Controllers
{
    public class Manager_StatisticsController : Controller
    {
        // GET: Home

        static P2Entities2 db = new P2Entities2();
       
        JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };


        public ActionResult index()
        {
            return View();
        }
        public ActionResult Column(int type, int category)
       {
            List<DataPoint> dataPoints = new List<DataPoint>();

            dataPoints = itemByType(type, category);

                try
                {
                    ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints.ToList(), _jsonSetting);

                    return View();
                }
                catch (System.Data.Entity.Core.EntityException)
                {
                    return View("Error");
                }
                catch (System.Data.SqlClient.SqlException)
                {
                    return View("Error");
                }
        
       
        }
       
       


        
        public ActionResult ExportChart(int type, int category)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();

            dataPoints = itemByTypePercent(type, category);
            try
            {
                ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints.ToList(), _jsonSetting);

                return View();
            }
            catch (System.Data.Entity.Core.EntityException)
            {
                return View("Error");
            }
            catch (System.Data.SqlClient.SqlException)
            {
                return View("Error");
            }
        }
        [HttpGet]
        public static List<DataPoint> itemByType(int type, int category)
        {
            string t = db.type_item.ToList().Find(a => a.id_type == type).name_type;
            string c = db.category_item.ToList().Find(a => a.id_category == category).name_category;

            List<myStatistics> i = new List<myStatistics>();
            if (type == 5)
            {
                i = db.myStatistics.ToList().FindAll(it => it.id_category == category);

            }
            else
            {
                i = db.myStatistics.ToList().FindAll(it => it.id_category == category && it.id_type == type);
            }
                List<DataPoint> dataPoints = new List<DataPoint>();
            item item_st = new item();
            DataPoint d = new DataPoint(0, item_st.name_item, i[0].id_item, i[0].size,t,c);
            dataPoints.Add(d);
            foreach (var item in i)
            {

                item_st = db.item.ToList().Find(ite => ite.id_item == item.id_item);
          
                {
                    if (dataPoints.ToList().Find(ist => ist.Z == item_st.id_item) != null)
                    {
                        dataPoints.ToList().Find(ist => ist.Z == item_st.id_item).Y= dataPoints.ToList().Find(ist => ist.Z == item_st.id_item).Y + item.amount;
                        dataPoints.ToList().Find(ist => ist.Z == item_st.id_item).Label = item_st.name_item;
                        int first = dataPoints.ToList().Find(ist => ist.Z == item_st.id_item).Size;
                        int neww = item.size;
                        if ( first< neww)
                        {
                            dataPoints.ToList().Find(ist => ist.Z == item_st.id_item).Size = neww;
                        }
                    }
                    else
                    {

                         d = new DataPoint(item.amount, item_st.name_item, item.id_item, item.size,t,c);
                        dataPoints.Add(d);

                    }
                }
            }
            

            return dataPoints;
        }

        public static List<DataPoint> itemByTypePercent(int type, int category)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();

            dataPoints = itemByType(type, category);
            var count = dataPoints.ToList().Sum(t => t.Y);
            foreach (var it in dataPoints)
            {
                var p= (it.Y)/count*100;
                dataPoints.ToList().Find(i => i.Y == it.Y).Y = p;

            }

            return dataPoints;
        }

    }
}


