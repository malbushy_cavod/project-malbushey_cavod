﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_salesController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: sales
        public ActionResult Index()
        {
            var sale = db.sale.Include(s => s.city).Include(s => s.neighborhood).Include(s => s.setting);
            return View(sale.ToList());
        }

       
        // GET: sales/Create
        public ActionResult Create()
        {
            ViewBag.idcity_sale = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            ViewBag.numitemssetting_sale = new SelectList(db.setting, "id_setting", "id_setting");
            return View();
        }

        // POST: sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_sale,name_sale,idcity_sale,idneighborhood_sale,fod_sale,lod_sale,date_sale,numitemssetting_sale,addressSale")] sale sale)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.sale.Add(sale);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.idcity_sale = new SelectList(db.city, "id_city", "name_city", sale.idcity_sale);
                ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", sale.idneighborhood_sale);
                ViewBag.numitemssetting_sale = new SelectList(db.setting, "id_setting", "id_setting", sale.numitemssetting_sale);
                return View(sale);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: sales/Edit/5
        public ActionResult Edit(int? id)
        {
           try{ if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sale sale = db.sale.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcity_sale = new SelectList(db.city, "id_city", "name_city", sale.idcity_sale);
            ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", sale.idneighborhood_sale);
            ViewBag.numitemssetting_sale = new SelectList(db.setting, "id_setting", "id_setting", sale.numitemssetting_sale);
            return View(sale);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // POST: sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_sale,name_sale,idcity_sale,idneighborhood_sale,fod_sale,lod_sale,date_sale,numitemssetting_sale,addressSale")] sale sale)
        {
            try{
                if (ModelState.IsValid)
            {
                db.Entry(sale).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcity_sale = new SelectList(db.city, "id_city", "name_city", sale.idcity_sale);
            ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", sale.idneighborhood_sale);
            ViewBag.numitemssetting_sale = new SelectList(db.setting, "id_setting", "id_setting", sale.numitemssetting_sale);
            return View(sale);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: sales/Delete/5
        public ActionResult Delete(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sale sale = db.sale.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: sales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try{
                sale sale = db.sale.Find(id);
                db.item_in_sale.RemoveRange(db.item_in_sale.Where(c => c.sale.id_sale == id));
                db.sale.Remove(sale);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }
        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {

            var res = db.sale.Where(a => a.name_sale.Contains(searchTxt) || a.city.name_city.ToString().Contains(searchTxt) || a.neighborhood.name_neighborhood.Contains(searchTxt) ).ToList();
            if (res.Count() != 0)
            {
                return PartialView(res);
            }
            else
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                return PartialView(res);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
