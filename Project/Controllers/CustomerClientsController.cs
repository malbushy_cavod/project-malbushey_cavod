﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Project.Models;
using System.Net.Mail;
using System.Text;

namespace Project.Controllers
{
    public class CustomerClientsController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: CustomerClients
        public ActionResult Index()
        {
            var client = db.client.Include(c => c.city).Include(c => c.neighborhood).Include(c => c.users);
            return View(client.ToList());
        }
        public ActionResult IndexFile()
        {
            var tz = Session["tzC"];
            client c = db.client.FirstOrDefault(a => a.tz_client == tz);
            var tblfiles = db.tblfiles.Where(t => t.id_c==c.id_client);
            return View(tblfiles.ToList());
        }
        // GET: CustomerClients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.client.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }
        //פונקציה לשליחת קוד לנרשם במייל 
        public void sendEmailAsync(string ToEmail, string UserName, string nPassword)/*string UniqeId(int id, string code)*/
        {
            try
            {
                MailMessage mail = new MailMessage(); //("organizationmc1@gmail.com", ToEmail"miriamyahud@neto.net.il", "מלבושי כבוד");
                mail.From = new MailAddress("organizationmc1@gmail.com", "מלבושי כבוד");
                mail.To.Add(ToEmail);
                StringBuilder sbEmailbody = new StringBuilder();
                sbEmailbody.Append("לכבוד " + UserName + ",<br/><br/>");
                sbEmailbody.Append("פרטיך נשמרו בהצלחה, הסיסמה שלך הינה " + nPassword);
                sbEmailbody.Append("<br/><br/>");
                sbEmailbody.Append("<b>מלבושי כבוד<b>");

                mail.IsBodyHtml = true;

                mail.Body = sbEmailbody.ToString();
                mail.Subject = " סיסמה למלבושי כבוד";
                SmtpClient smtpClient = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential("organizationmc1@gmail.com", "123456789mc")
                };
                smtpClient.Send(mail);
            }
            catch
            { }
        }
        // GET: CustomerClients/Create
        public ActionResult Create()
        {
            ViewBag.name = Session["name"];
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
            return View();
        }

        // POST: CustomerClients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create([Bind(Include = "id_client,tz_client,iduser_client,address_client,idcity_client,idneighborhood_client,phone_client,avgsalary_client,email_client,nameclient,numchildren_client,lastorder_client,salary1,salary2,salary3,salary4")]
        client client, string fjob, string mjob)
        {
            try
            {
                if (fjob == "1")
                    fjob = "לא עובד";
                else
                    fjob = "עובד";
                if (mjob == "1")
                    mjob = "לא עובד";
                else
                    mjob = "עובד";
                //רישום של משתמש חדש
                client.fatherjob_client = fjob;
                client.motherjob_client = mjob;
                int minFamsize = db.setting.Min(mi => mi.minfamsize_setting);
                int maxSallary = db.setting.Max(ma => ma.maxsalary_setting);
                var fatherjob = db.setting.Max(f => f.fatherjob_setting);
                client.avgsalary_client = (Convert.ToInt16(client.salary1) + Convert.ToInt16(client.salary2) + Convert.ToInt16(client.salary3) + Convert.ToInt16(client.salary4)) / 4;
                var c = client.avgsalary_client / client.numchildren_client;
                if ((client.avgsalary_client < maxSallary && client.numchildren_client > minFamsize) && (client.fatherjob_client == "לא עובד"||client.motherjob_client=="לא עובד"  && c < 2500)||
                    (client.fatherjob_client == "עובד" && client.motherjob_client == "עובד"  && c < 1900))
                {
                    client.tz_client =Session["tz"].ToString();
                    client.passwordc = Session["password"].ToString();
                    client.nameclient = Session["name"].ToString();
                    client.iduser_client = 4;
                    db.client.Add(client);
                    db.SaveChanges();
                    Session.Add("idC",client.id_client);
                    sendEmailAsync(client.email_client, client.nameclient, client.passwordc);
                    Session.Add("nameC", Session["name"].ToString());
                    ViewBag.name = Session["name"].ToString();
                    //שליחת רשימת ילדים למילוי לפי מספר ילדים שהמשתמש הכניס
                    List<children> listChildren = new List<children>();
                    for (int i = 0; i < client.numchildren_client; i++)
                    {
                        children children = new children();
                        children.idclient_child = client.id_client;
                        listChildren.Add(children);
                    }

                    CustomerClientVM ccVM = new CustomerClientVM();

                    ccVM.listChlidren = listChildren;
                    ViewBag.name = Session["nameC"].ToString();

                    return View("CreateDetails", ccVM);
                }
                else
                {
                    ViewBag.name = Session["name"];
                    ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
                    ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                    ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                    ViewBag.error = "מצטערים אך אינך עונה לקריטריונים של הארגון";
                    return View();
                }
            }
            catch
            {
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                return View(client);
            }

            
        }

// GET: CustomerClients/Edit/5
public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.client.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
            return View(client);
        }

        // POST: CustomerClients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_client,iduser_client,address_client,idcity_client,idneighborhood_client,phone_client,avgsalary_client,email_client,nameclient,lastorder_client,numchildren_client,tz_client,passwordc,salary1,salary2,salary3,salary4")] client client, string fjob, string mjob)
        {
            int check = 0;
            if (fjob == "1")
                fjob = "לא עובד";
            else
                fjob = "עובד";
            if (mjob == "1")
                mjob = "לא עובד";
            else
                mjob = "עובד";
            client.fatherjob_client = fjob;
            client.motherjob_client = mjob;
            if (ModelState.IsValid)
            {  
                //עדכון פרטים של משתמש קיים
                int maxSallary = db.setting.Max(ma => ma.maxsalary_setting);
                int minFamsize = db.setting.Min(mi => mi.minfamsize_setting);
                var fatherjob = db.setting.Max(f => f.fatherjob_setting);
                client.avgsalary_client = (Convert.ToInt16(client.salary1) + Convert.ToInt16(client.salary2) + Convert.ToInt16(client.salary3) + Convert.ToInt16(client.salary4)) / 4;
                var c = client.avgsalary_client / client.numchildren_client;
                if ((client.avgsalary_client < maxSallary && client.numchildren_client > minFamsize) && (client.fatherjob_client == "לא עובד" || client.motherjob_client == "לא עובד" && c < 2500) ||
                   (client.fatherjob_client == "עובד" && client.motherjob_client == "עובד" && c < 1900))
                {
                    var entity = db.client.Where(b => b.id_client == client.id_client).AsQueryable().FirstOrDefault();
                    if (entity.salary1 != client.salary1 || entity.salary2 != client.salary2 || entity.salary3 != client.salary3 || entity.fatherjob_client != client.fatherjob_client)
                        check = 1;  //משתמש שינה את המשכורות שלו או את עיסוק האב
                    if (entity.numchildren_client != client.numchildren_client)
                        check = 2;//משתמש שינה את מספר ילדיו
                    db.Entry(entity).CurrentValues.SetValues(client);
                    db.SaveChanges();
                    if (check == 1)//עדכון קבצים
                    {
                        var tblfiles = db.tblfiles.Where(t => t.client.id_client == client.id_client).ToList();
                        return View("IndexFile", tblfiles);
                    }
                    else if (check == 2)//עדכון פרטי ילדים
                    {
                        // :בדיקה אם מספר ילדים בטבלת קלינט גדול ממספר הילדים המופיעים בטבלת ילדים כלומר 
                        // משתמש לא סיים את תהליך הרישום-הזין מספר ילדים אך יצא מהתכנית בלי להזין אותם בטבלת ילדים 
                        //מעבר להוספת ילדים במידה והכניסו מספר ילדים גדול ממה ששמור במערכת
                        var nChild = db.children.Count(a => a.idclient_child == client.id_client);
                        if (nChild < client.numchildren_client)
                        {
                            List<children> ezChild = new List<children>();
                            ezChild = db.children.Where(v => v.idclient_child == client.id_client).ToList();
                            List<children> listChildren = new List<children>();
                            for (int i = ezChild.Count(); i < client.numchildren_client; i++)
                            {
                                children children = new children();
                                children.idclient_child = client.id_client;
                                listChildren.Add(children);
                            }
                            CustomerClientVM ccVM = new CustomerClientVM();

                            ccVM.listChlidren = listChildren;
                            return View("CreateDetails", ccVM);
                        }
                        else
                        {
                            //מעבר להסרת ילדים שמעל גיל 18 במידה והכניסו מספר ילדים קטן ממה ששמור במערכת
                            if (nChild > client.numchildren_client)
                            {
                                int moneDel = 0;
                                var lChild = db.children.Where(a => a.idclient_child == client.id_client).ToList();
                                for (int i = 0; i < nChild; i++)
                                {
                                    int age = DateTime.Now.Year - lChild[i].birthdate_child.Year;
                                    if (age > 18)
                                    {
                                        moneDel++;
                                        db.children.Remove(db.children.Find(lChild[i].id_child));
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        if (age == 18)
                                        {
                                            if (DateTime.Now.Month < lChild[i].birthdate_child.Month || DateTime.Now.Month == lChild[i].birthdate_child.Month && DateTime.Now.Day < lChild[i].birthdate_child.Day)
                                            {
                                                moneDel++;
                                                db.children.Remove(db.children.Find(lChild[i].id_child));
                                                db.SaveChanges();
                                            }
                                        }

                                    }

                                }//במידה והמערכת זיהתה מספר ילדים מעל גיל 18 גדול יותר ממה שהמשתמש הזין 
                                if (moneDel > nChild - client.numchildren_client)
                                {
                                    var entity1 = db.client.Where(b => b.id_client == client.id_client).AsQueryable().FirstOrDefault();
                                    client.numchildren_client = nChild - moneDel;
                                    db.Entry(entity1).CurrentValues.SetValues(c);
                                }
                                if (moneDel == 0)
                                {    //אם לא מצא ילדים מתחת גיל 18
                                    ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
                                    ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                                    ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                                    ViewBag.error = "מס' ילדים מעל גיל 18 שגוי";
                                    return View("Edit", client);
                                }
                            }//אם אינו עומד בקרקטריונים לאחר עדכון הפרטים
                            int minFamsize1 = db.setting.Min(mi => mi.minfamsize_setting);
                            if (client.numchildren_client > minFamsize1)
                            {
                                ViewBag.error = "מצטערים אך אינך עונה לקריטריונים של הארגון";
                                client client1 = db.client.Find(client.id_client);
                                if (client == null)
                                {
                                    return HttpNotFound();
                                }
                                ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
                                ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                                ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                                return View(client1);
                            }
                        }                       
                    }

                    else
                        return RedirectToAction("ContactUs", "Manager_items");
                }
                else
                {
                    ViewBag.error = "מצטערים אך אינך עונה לקריטריונים של הארגון";
                    client client1 = db.client.Find(client.id_client);
                    if (client == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
                    ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                    ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                    return View(client1);
                }
            }
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
            return View(client);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDetails([Bind(Include = "email_client,address_client,idcity_client,idneighborhood_client,phone_client")]client client, string name)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(client);
        }

        // GET: CustomerClients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.client.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: CustomerClients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            client client = db.client.Find(id);
            db.client.Remove(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        // GET: CustomerClients
        // GET: /Edit/5
        public ActionResult EditFile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblfiles tblfiles = db.tblfiles.Find(id);
            if (tblfiles == null)
            {
                return HttpNotFound();
            }
            return View(tblfiles);
        }

        [HttpPost]//שינוי מסמך קיים לרשום ע"י משתמש
        public ActionResult EditFile(HttpPostedFileBase postedFiles, int id)
        {
                                                       
            byte[] bytes;
            var file = db.tblfiles.Find(id);
            var id_cl = id;
            if (postedFiles == null)
            {
                ViewBag.error3 = "לא צרפת/ת את כל המסמך הנדרש";
                return View();
            }

            try
            {
                //שמירת הקובץ בשם המתאים -ע"י המרתם לבינארי  
                using (BinaryReader br = new BinaryReader(postedFiles.InputStream))
                {
                    bytes = br.ReadBytes(postedFiles.ContentLength);
                }
                string allf = Path.GetFileName(postedFiles.FileName);
                string end = allf.Substring(allf.IndexOf("."), allf.Length - allf.IndexOf("."));
                string endf = file.namef.Substring(file.namef.IndexOf("."), file.namef.Length - file.namef.IndexOf("."));

                string start = file.namef.Substring(0, file.namef.Length - endf.Length);
                file.namef = start + end;
                file.dataf = bytes;
                file.contentType = postedFiles.ContentType;
                db.Entry(file).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.eror3 = "";
            }
            catch
            {
                ViewBag.eror3 = "ארעה שגיאה בעת העלאת הקבצים - נסה שוב";
                return View();
            }
            var tblfiles = db.tblfiles.Where(t => t.client.id_client ==file.id_c).ToList();
            return View("IndexFile", tblfiles);
        }
        // GET: /Delete/5
        public ActionResult DeleteFile(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblfiles tblfiles = db.tblfiles.Find(id);
                if (tblfiles == null)
                {
                    return HttpNotFound();
                }
                return View(tblfiles);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: /Delete/5
        [HttpPost, ActionName("DeleteFile")]//מחיקת קבצים מצורפים
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedFile(int id)
        {
            try
            {
                tblfiles tblfiles = db.tblfiles.Find(id);
                var idc = tblfiles.id_c;
                db.tblfiles.Remove(tblfiles);
                db.SaveChanges();
                var tblfiles1 = db.tblfiles.Where(t => t.client.id_client == idc).ToList();
                return View("IndexFile", tblfiles1);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

