﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_item_in_saleController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: item_in_sale
        public ActionResult Index()
        {
            var item_in_sale = db.item_in_sale.Include(i => i.city).Include(i => i.sale).Include(i => i.item);
            return View(item_in_sale.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = db.size.Where(t => t.id_item == id).ToList();
            if (item.Count() == 0)
            {
                //הצגת המידות לכל פריט מטבלת מידות
                var item1 = db.item_in_sale.Find(id);
                List<size> size = new List<size>();
                size ez = new size();
                ez.item_in_sale = item1;
                size.Add(ez);
                if (item1 == null)
                {
                    return HttpNotFound();

                }
                return View(size);
            }

            return View(item);
        }
        // GET: item_in_sale/Create
        public ActionResult Create()
        {
            ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            //ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale");
            ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "id_sale");
            ViewBag.iditems = new SelectList(db.item, "id_item", "name_item");
            return View();
        }

        // POST: item_in_sale/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_itemsale,itsale_itemsale,iditem_itemsale,size_itemsale,amountinstock_itemsale,iditems")] item_in_sale item_in_sale)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    item_in_sale.size_itemsale = 0;
                    item_in_sale.amountinstock_itemsale = 0;
             
                    db.item_in_sale.Add(item_in_sale);
                    db.SaveChanges();
                    //הוספת כמות לכל מידה שהכניסו
                    List<int> s = new List<int>();
                    int index1 = db.item.Find(item_in_sale.iditems).sizeinstock_item.IndexOf('-');
                    if (index1 != -1)
                    {
                        string start = item_in_sale.item.sizeinstock_item.Substring(0, item_in_sale.item.sizeinstock_item.LastIndexOf("-"));
                        string end = item_in_sale.item.sizeinstock_item.Remove(0, item_in_sale.item.sizeinstock_item.LastIndexOf("-") + 1);
                        if (item_in_sale.item.id_i_category != 4)
                        {// מידות לפרטי לבוש זוגי בלבד
                            if (Convert.ToInt16(start) % 2 != 0 || Convert.ToInt16(end) % 2 != 0)
                            {
                                ViewBag.error1 = "נא הכנס מידות זוגיות בלבד";
                                return View(item_in_sale);
                            }
                            for (int i = Convert.ToInt16(start); i <= Convert.ToInt16(end); i += 2)
                            {
                                s.Add(i);
                            }
                        }
                        else
                        {//מידות לנעליים
                            for (int i = Convert.ToInt16(start); i <= Convert.ToInt16(end); i++)
                            {
                                s.Add(i);
                            }
                        }

                    }
                    else
                        s.Add(Convert.ToInt16(item_in_sale.item.sizeinstock_item));
                    List<size> listSize = new List<size>();
                    for (int i = 0; i < s.Count(); i++)
                    {
                        size size = new size();
                        size.n_size = s[i];
                        size.id_item = item_in_sale.id_itemsale;
                        listSize.Add(size);
                    }
                    CustomerClientVM ccVM = new CustomerClientVM();

                    ccVM.listSize = listSize;
                    return View("~/Views/Manager_sizes/Create.cshtml", ccVM);
                }

                ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city");
                ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
               // ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale");
                ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "id_sale");
                ViewBag.iditems = new SelectList(db.item, "id_item", "name_item");
                return View(item_in_sale);
        } 
                    catch
                    {
                ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city");
                ViewBag.idneighborhood_sale = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                //ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale");
                ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "id_sale");
                ViewBag.iditems = new SelectList(db.item, "id_item", "name_item");
                ViewBag.error = ".ארעה שגיאה בשמירת הפרטים";
                        return View();
                     }
}

// GET: item_in_sale/Edit/5
public ActionResult Edit(int? id)
{
    try
    {
        if (id == null)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        item_in_sale item_in_sale = db.item_in_sale.Find(id);
        if (item_in_sale == null)
        {
            return HttpNotFound();
        }
        ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city", item_in_sale.itsale_itemsale);
        ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale", item_in_sale.iditem_itemsale);
        ViewBag.iditems = new SelectList(db.item, "id_item", "name_item", item_in_sale.iditems);
        return View(item_in_sale);
    }
    catch
    {
        ViewBag.error = ".ארעה שגיאה בעדכון הפרטים";
        return View();
    }
}

        ////        // POST: item_in_sale/Edit/5
        ////        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        ////        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_itemsale,itsale_itemsale,iditem_itemsale,size_itemsale,amountinstock_itemsale,iditems")] item_in_sale item_in_sale)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int sw = 0;
                    var ez = db.item.Find(item_in_sale.item.id_item);
                    if (ez.sizeinstock_item != item_in_sale.item.sizeinstock_item)
                    {
                        sw = 1;//השתנו המידות
                    }
                    ez.sizeinstock_item = item_in_sale.size_itemsale.ToString();
                    var entity = db.item_in_sale.Where(b => b.id_itemsale == item_in_sale.id_itemsale).AsQueryable().FirstOrDefault();
                    db.Entry(entity).CurrentValues.SetValues(item_in_sale);
                     var entity1 = db.item.Where(b => b.id_item == ez.id_item).AsQueryable().FirstOrDefault();
                    db.Entry(entity1).CurrentValues.SetValues(ez);
                    db.SaveChanges();
                    if (sw == 1)
                    {
                        //אם הזינו מידות חדשות - מחיקת המידות הישנות מטבלת מידות והזנת כמויות למידות החדשות שהוזנו
                        db.size.RemoveRange(db.size.Where(c => c.item_in_sale.id_itemsale == item_in_sale.id_itemsale));
                        db.SaveChanges();
                        List<int> s = new List<int>();
                        int index1 = item_in_sale.item.sizeinstock_item.IndexOf('-');
                        if (index1 != -1)
                        {
                            string start = item_in_sale.item.sizeinstock_item.Substring(0, item_in_sale.item.sizeinstock_item.LastIndexOf("-"));
                            string end = item_in_sale.item.sizeinstock_item.Remove(0, item_in_sale.item.sizeinstock_item.LastIndexOf("-") + 1);
                            if (item_in_sale.item.id_i_category != 4)
                            {// מידות לפרטי לבוש זוגי בלבד
                                if (Convert.ToInt16(start) % 2 != 0 || Convert.ToInt16(end) % 2 != 0)
                                {
                                    ViewBag.error1 = "נא הכנס מידות זוגיות בלבד";
                                    return View(item_in_sale);
                                }
                                for (int i = Convert.ToInt16(start); i <= Convert.ToInt16(end); i += 2)
                                {
                                    s.Add(i);
                                }
                            }
                            else
                            {//מידות לנעליים
                                for (int i = Convert.ToInt16(start); i <= Convert.ToInt16(end); i++)
                                {
                                    s.Add(i);
                                }
                            }

                        }
                        else //מוסיף מידות חדשות
                            s.Add(Convert.ToInt16(item_in_sale.item.sizeinstock_item));
                        List<size> listSize = new List<size>();
                        for (int i = 0; i < s.Count(); i++)
                        {
                            size size = new size();
                            size.n_size = s[i];
                            size.item_in_sale.id_itemsale = item_in_sale.id_itemsale;
                            listSize.Add(size);
                        }
                        CustomerClientVM ccVM = new CustomerClientVM();

                        ccVM.listSize = listSize;
                        return View("~/Views/Manager_sizes/Create.cshtml", ccVM);
                    }
                    return RedirectToAction("Index");
                }
                ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city", item_in_sale.itsale_itemsale);
                ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale", item_in_sale.iditem_itemsale);
                ViewBag.iditems = new SelectList(db.item, "id_item", "name_item", item_in_sale.iditems);
                return View(item_in_sale);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: item_in_sale/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                item_in_sale item_in_sale = db.item_in_sale.Find(id);
                if (item_in_sale == null)
                {
                    return HttpNotFound();
                }
                return View(item_in_sale);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: item_in_sale/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                item_in_sale item_in_sale = db.item_in_sale.Find(id);
                db.size.RemoveRange(db.size.Where(c => c.item_in_sale.iditems == id));
                db.item_in_sale.Remove(item_in_sale);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {

            var res = db.item_in_sale.Where(a => a.city.name_city.Contains(searchTxt) || a.item.name_item.Contains(searchTxt) || a.sale.name_sale.Contains(searchTxt)).ToList();
            if (res.Count() != 0)
            {
                return PartialView(res);
            }
            else
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                return PartialView(res);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
   