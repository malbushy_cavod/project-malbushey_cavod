﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_citiesController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_cities
        public ActionResult Index()
        {
            return View(db.city.ToList());
        }

     

        // GET: Manager_cities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manager_cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_city,name_city")] city city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.city.Add(city);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(city);
            } 
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_cities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            city city = db.city.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // POST: Manager_cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_city,name_city")] city city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(city);
        }

        // GET: Manager_cities/Delete/5
        public ActionResult Delete(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            city city = db.city.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try {
                city city = db.city.Find(id);
                db.item_in_sale.RemoveRange(db.item_in_sale.Where(c => c.city.id_city == id));
                db.item_in_wait.RemoveRange(db.item_in_wait.Where(c => c.client.city.id_city == id));
                db.order_details.RemoveRange(db.order_details.Where(c => c.orders.client.city.id_city == id));
                db.orders.RemoveRange(db.orders.Where(c => c.client.city.id_city == id));
                db.tblfiles.RemoveRange(db.tblfiles.Where(c => c.client.city.id_city == id));
                db.children.RemoveRange(db.children.Where(c => c.client.city.id_city == id));
                db.client.RemoveRange(db.client.Where(c => c.city.id_city == id));
                db.sale.RemoveRange(db.sale.Where(c => c.city.id_city == id));
                db.neighborhood.RemoveRange(db.neighborhood.Where(c => c.city.id_city == id));
                db.city.Remove(city);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
