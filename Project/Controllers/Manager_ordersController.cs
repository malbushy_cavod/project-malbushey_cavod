﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_ordersController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_orders 
        public ActionResult Index()
        {
            var orders = db.orders.Include(o => o.client);
            return View(orders.ToList());
        }

        //// GET: Manager_orders/Create

        // POST: Manager_orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_order,idclient_order,date_order,idorder_detail")] orders orders,string id_client)
        {
            try
            {
                //int i = Convert.ToInt16(id_client);
                //var cl = db.client.Find(i);
                //orders.client = cl;
                orders.client = db.client.Find(Session.SessionID); ;
                if (ModelState.IsValid)
                {
                    db.orders.Add(orders);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            
                ViewBag.id_client = new SelectList(db.client, "id_client", "tz_client");
                ViewBag.idclient_order = new SelectList(db.client, "id_client", "address_client", orders.idclient_order);
                return View();
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_orders/Edit/5
        public ActionResult Edit(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orders orders = db.orders.Find(id);
            if (orders == null)
            {
                return HttpNotFound();
            }
            ViewBag.idclient_order = new SelectList(db.client, "id_client", "address_client", orders.idclient_order);
            return View(orders);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }

        }

        // POST: Manager_orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_order,idclient_order,date_order,idorder_detail")] orders orders)
        {
           try{ if (ModelState.IsValid)
            {
                db.Entry(orders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idclient_order = new SelectList(db.client, "id_client", "address_client", orders.idclient_order);
            return View(orders);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: Manager_orders/Delete/5
        public ActionResult Delete(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orders orders = db.orders.Find(id);
            if (orders == null)
            {
                return HttpNotFound();
            }
            return View(orders);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try{
                orders orders = db.orders.Find(id);
                db.order_details.RemoveRange(db.order_details.Where(c => c.orders.id_order == id));
                db.orders.Remove(orders);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }
        //חיפוס פריט לפי שם 
        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {
            int idO ;
            bool isNumerical = int.TryParse(searchTxt, out idO);
            if (isNumerical)
            {
                var res = db.orders.Where(a => a.client.nameclient.Contains(searchTxt) || a.client.tz_client.Contains(searchTxt) || a.id_order == idO).ToList();
                if (res.Count() != 0)
                {
                    return PartialView(res);
                }
                else
                {
                    ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                    return PartialView(res);
                }
            }
            else
            {
             var res = db.orders.Where(a => a.client.nameclient.Contains(searchTxt) || a.client.tz_client.Contains(searchTxt)).ToList();
                if (res.Count() != 0)
                {
                    return PartialView(res);
                }
                else
                {
                    ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                    return PartialView(res);
                }
            }
                

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
