﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_tblfilesController : Controller
    {
        private P2Entities2 db = new P2Entities2();
        

        // GET: Manager_tblfiles
        public ActionResult Index()
        {
            var tblfiles = db.tblfiles.Include(t => t.client);
            return View(tblfiles.ToList());
        }

     
        //פונקציה להורדת קובץ מהאתר
        [HttpPost]
        public FileResult DownloadFile(int? fileId)
        {
         
            tblfiles file = db.tblfiles.ToList().Find(p => p.id == fileId.Value);
            return File(file.dataf, file.contentType, file.namef);
        }

        // GET: Manager_tblfiles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload_files(List<HttpPostedFileBase> postedFiles, List<HttpPostedFileBase> postedFiles1, List<HttpPostedFileBase> postedFiles2, string tzt)
        {

            byte[] bytes;
            var fName = "";
            object tz = "";
            
            if (tzt != null&& tzt!="check")
            {
                tz = tzt;              
            }
            else
            { 
                tz = Session["tzC"];
            }
            if(tz==null)
                tz = Session["tz"];
            var cl = db.client.Where(a => a.tz_client == tz);
            var count = cl.Count();
            var i = 1;
            if (count == 0)
            {
                ViewBag.error3 = "ת'ז לא קיימת במערכת";
                return View("Create");
            }
            var id_cl = cl.First().id_client;
            if ((postedFiles[0] == null || postedFiles1[0] == null || postedFiles[0] == null || id_cl == null)&&tzt!="check")
            {
                ViewBag.error3 = "לא צרפת/ה את כל המסמכים הנדרשים";
                if (Session["tzt"] != "")//הוספת קובץ ע"י משתמש חדש
                {
                    Session["tzt"] = null;
                    return View(); 
                }
                return RedirectToAction("Create", "CustomerClients");
               
            }
            try
            {
                if (postedFiles[0] != null)
                {
                    foreach (HttpPostedFileBase postedFile in postedFiles)
                    { //שמירת הקבצים בשם המתאים -ע"י המרתם לבינארי  
                        using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                        {
                            bytes = br.ReadBytes(postedFile.ContentLength);
                        }
                        string end = Path.GetFileName(postedFile.FileName);
                        end = end.Substring(end.IndexOf("."), end.Length - end.IndexOf("."));
                        string fileName = tz.ToString() + i + "תז" + fName.ToString() + end;
                        db.tblfiles.Add(new tblfiles
                        {
                            namef = fileName,
                            contentType = postedFile.ContentType,
                            id_c = id_cl,
                            dataf = bytes

                        });
                        i++;
                    }

                }
                i = 1;
                if (postedFiles1[0] != null)
                {
                    foreach (HttpPostedFileBase postedFile in postedFiles1)
                    {//שמירת הקבצים בשם המתאים -ע"י המרתם לבינארי  
                        using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                        {
                            bytes = br.ReadBytes(postedFile.ContentLength);
                        }
                        string end = Path.GetFileName(postedFile.FileName);
                        end = end.Substring(end.IndexOf("."), end.Length - end.IndexOf("."));
                        string fileName = tz.ToString() + i + "עיסוק האב" + fName.ToString() + end;
                        db.tblfiles.Add(new tblfiles
                        {
                            namef = fileName,
                            contentType = postedFile.ContentType,
                            id_c = id_cl,
                            dataf = bytes

                        });

                        i++;
                    }
                }

                i = 1;
                if (postedFiles2[0] != null)
                {
                    foreach (HttpPostedFileBase postedFile in postedFiles2)
                    {
                        //שמירת הקבצים בשם המתאים -ע"י המרתם לבינארי  
                        using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                        {
                            bytes = br.ReadBytes(postedFile.ContentLength);
                        }
                        string end = Path.GetFileName(postedFile.FileName);
                        end = end.Substring(end.IndexOf("."), end.Length - end.IndexOf("."));
                        string fileName = tz.ToString() + i + "תלוש" + fName.ToString() + end;
                        db.tblfiles.Add(new tblfiles
                        {
                            namef = fileName,
                            contentType = postedFile.ContentType,
                            id_c = id_cl,
                            dataf = bytes

                        });
                        i++;
                    }
                }
                db.SaveChanges();
                ViewBag.eror3 = "";
                if (tzt != null && tzt!="check")
                {
                   return RedirectToAction("Index");//במידה והמנהל הוסיף קובץ
                }
                    return RedirectToAction("ContactUs", "Manager_items");//במידה ומשתמש הוסיף קובץ
            }
            catch
            {
                ViewBag.eror3 = "ארעה שגיאה בעת העלאת הקבצים - נסה שוב";
                    return View();

            }
           
           
        }

        // GET: Manager_tblfiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblfiles tblfiles = db.tblfiles.Find(id);
            if (tblfiles == null)
            {
                return HttpNotFound();
            }
            return View(tblfiles);
        }

        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase postedFiles,int id)
        {
            byte[] bytes;
            var file = db.tblfiles.Find(id);
            var id_cl = id;
                if (postedFiles == null)
            {
                ViewBag.error3 = "לא צרפת/ה את המסמך הנדרש";
                return View();
            }

            try
            {//שמירת הקובץ המעודכן בשם המתאים -ע"י המרתם לבינארי 
                using (BinaryReader br = new BinaryReader(postedFiles.InputStream))
                    {
                        bytes = br.ReadBytes(postedFiles.ContentLength);
                    }
                string allf = Path.GetFileName(postedFiles.FileName);
                string end = allf.Substring(allf.IndexOf("."), allf.Length - allf.IndexOf("."));
                string endf = file.namef.Substring(file.namef.IndexOf("."), file.namef.Length - file.namef.IndexOf("."));

                string start = file.namef.Substring(0, file.namef.Length - endf.Length);
                file.namef = start + end;
                file.dataf = bytes;
                file.contentType = postedFiles.ContentType;
                db.Entry(file).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.eror3 = "";
            }
            catch
            {
                ViewBag.eror3 = "ארעה שגיאה בעת העלאת הקבצים - נסה שוב";
                    return View();
            }
            return RedirectToAction("Index");
        }

        // GET: Manager_tblfiles/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblfiles tblfiles = db.tblfiles.Find(id);
            if (tblfiles == null)
            {
                return HttpNotFound();
            }
            return View(tblfiles);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_tblfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { tblfiles tblfiles = db.tblfiles.Find(id);
            db.tblfiles.Remove(tblfiles);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }
        // חיפוס קובץ לפי שם רשום /ת"ז רשום 
        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {

            var res = db.tblfiles.Where(a => a.client.nameclient.Contains(searchTxt) || a.client.tz_client.Contains(searchTxt)).ToList();
            if (res.Count() != 0)
            {
                return PartialView(res);
            }
            else
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                return PartialView(res);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
