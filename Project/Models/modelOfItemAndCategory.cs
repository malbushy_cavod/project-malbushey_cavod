﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class modelOfItemAndCategory
    {
        public IEnumerable<item_in_sale> Items { get; set; }
        public IEnumerable<category_item> Category_itemsMan { get; set; }
        public IEnumerable<category_item> Category_itemsWoman { get; set; }
        public List<MyShoppingCart> MyShoppingCart { get; set; }
        public List<orders> orders { get; set; }
        public List<order_details> order_details { get; set; }
        public List<item_in_wait> item_in_wait { get; set; }
    }
}