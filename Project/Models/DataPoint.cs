using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Project.Models
{
    //DataContract for Serializing Data - required to serve in JSON format
    [DataContract]
    public class DataPoint
    {
        public DataPoint(double y, string label)
        {
            this.Y = y;
            this.Label = label;
        }
        public DataPoint(double y, string label,int id_item,int size,string type,string category)
        {
            this.Y = y;
            this.Label = label;
            this.Z = id_item;
            this.Size = size;
            this.Type = type;
            this.Category = category;
        }

        public DataPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }


        public DataPoint(double x, double y, string label)
        {
            this.X = x;
            this.Y = y;
            this.Label = label;
        }

        public DataPoint(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public DataPoint(double x, double y, double z, string label)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.Label = label;
        }

        public DataPoint(string v1, int v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }

        public DataPoint()
        {
        }


        //Explicitly setting the name to be used while serializing to JSON. 
        [DataMember(Name = "label")]
        public string Label = null;
        [DataMember(Name = "color")]
        public string Color = null;
        [DataMember(Name = "size")]
        public int Size = 0;
        [DataMember(Name = "type")]
        public string Type = null;
        [DataMember(Name = "category")]
        public string Category = null;
        //Explicitly setting the name to be used while serializing to JSON.
        [DataMember(Name = "y")]
        public Nullable<double> Y = null;

        //Explicitly setting the name to be used while serializing to JSON.
        [DataMember(Name = "x")]
        public Nullable<double> X = null;

        //Explicitly setting the name to be used while serializing to JSON.
        [DataMember(Name = "z")]
        public Nullable<double> Z = null;
        private string v1;
        private int v2;
    }
}